// two_step_barrier.cpp
#include "upcxx_utils/promise_collectives.hpp"

#include <cassert>

#include "upcxx_utils/log.hpp"
#include "upcxx_utils/timers.hpp"

using upcxx::future;
using upcxx::make_future;
using upcxx::when_all;

namespace upcxx_utils {

int roundup_log2(uint64_t n) {
  // DBG("n=", n, "\n");
  if (n == 0) return -1;
#define S(k)                     \
  if (n >= (UINT64_C(1) << k)) { \
    i += k;                      \
    n >>= k;                     \
  }

  n--;
  // DBG(" n=", n, "\n");
  n |= n >> 1;
  // DBG(" n=", n, "\n");
  n |= n >> 2;
  // DBG(" n=", n, "\n");
  n |= n >> 4;
  n |= n >> 8;
  n |= n >> 16;
  n |= n >> 32;
  n++;
  // DBG(" n=", n, "\n");

  int i = 0;
  S(32);
  S(16);
  S(8);
  S(4);
  S(2);
  S(1);

  // DBG(" n=", n, " i=", i, "\n");
  return i;
#undef S
}

PromiseBarrier::DisseminationWorkflow::DisseminationWorkflow()
    : level_proms()  // empty
    , initiated_prom()
    , done_future()  // never ready
{}

void PromiseBarrier::DisseminationWorkflow::init_workflow(DistDisseminationWorkflow &dist_dissem) {
  DBG_VERBOSE("\n");
  assert(upcxx::master_persona().active_with_caller());

  DisseminationWorkflow &dissem = *dist_dissem;
  assert(dissem.level_proms.size() == 0);

  const upcxx::team &tm = dist_dissem.team();
  intrank_t me = tm.rank_me(), n = tm.rank_n();
  int levels = roundup_log2(n);
  dissem.level_proms.resize(levels);

  intrank_t mask = 0x01;
  future<> fut_chain = dissem.initiated_prom.get_future();
  for (int level = 0; level < levels; level++) {
    intrank_t send_to = (me + mask) % n;

    fut_chain = fut_chain.then([&dist_dissem, &tm, send_to, level]() {
      rpc_ff(
          tm, send_to,
          [](DistDisseminationWorkflow &dist_dissem, int level) {
            auto &prom = dist_dissem->level_proms[level];
            prom.fulfill_anonymous(1);
          },
          dist_dissem, level);
    });

    auto &prom = dissem.level_proms[level];
    fut_chain = when_all(fut_chain, prom.get_future());
    mask <<= 1;
  }
  dissem.done_future = fut_chain.then([&dissem]() {
    vector<upcxx::promise<>> empty{};
    dissem.level_proms.swap(empty);  // cleanup memory
  });
}

upcxx::future<> PromiseBarrier::DisseminationWorkflow::get_future() const { return done_future; }

PromiseBarrier::PromiseBarrier(const upcxx::team &tm)
    : tm(tm)
    , dist_workflow(tm) {
  DBG_VERBOSE("tm.n=", tm.rank_n(), " this=", this, "\n");
  assert(upcxx::master_persona().active_with_caller());
  DisseminationWorkflow::init_workflow(dist_workflow);
}

PromiseBarrier::PromiseBarrier(PromiseBarrier &&mv)
    : tm(mv.tm)
    , dist_workflow(std::move(mv.dist_workflow)) {
  DBG_VERBOSE("moved mv=", &mv, " to this=", this, "\n");
  mv.moved = true;
}

PromiseBarrier &upcxx_utils::PromiseBarrier::operator=(PromiseBarrier &&mv) {
  PromiseBarrier newme(std::move(mv));
  std::swap(*this, newme);
  DBG_VERBOSE("Swapped newme=", &newme, " to this=", this, "\n");
  return *this;
}

PromiseBarrier::~PromiseBarrier() {
  DBG_VERBOSE("Destroy this=", this, " move=", moved, "\n");
  if (moved) return;  // invalidated
  assert(upcxx::master_persona().active_with_caller());
  assert(dist_workflow->initiated_prom.get_future().ready());
  get_future().wait();
}

void PromiseBarrier::fulfill() const {
  DBG_VERBOSE("fulfill this=", this, "\n");
  assert(upcxx::master_persona().active_with_caller());
  assert(!dist_workflow->initiated_prom.get_future().ready());
  dist_workflow->initiated_prom.fulfill_anonymous(1);
}

upcxx::future<> PromiseBarrier::get_future() const {
  assert(upcxx::master_persona().active_with_caller());
  return dist_workflow->get_future();
}

// PromiseReduce class and helper functor struct

op_PromiseReduce::op_PromiseReduce(op_PromiseReduce::Funcs &ops)
    : _ops(ops) {
  assert(!upcxx::in_progress());
}
op_PromiseReduce::op_PromiseReduce(const op_PromiseReduce &copy)
    : _ops(copy._ops) {}

double &op_PromiseReduce::T2double(op_PromiseReduce::T &x) {
  // Just mash the int64_t bits into a double
  assert(sizeof(T) == sizeof(double));
  void *t_ptr = &x;
  double *d_ptr = (double *)t_ptr;
  double &d = *d_ptr;
  // DBG_VERBOSE("T2double x=", x, " d=", d, "\n");
  return d;
}

op_PromiseReduce::T &op_PromiseReduce::double2T(double &x) {
  // Just mash the double bits into a int64_t
  assert(sizeof(T) == sizeof(double));
  void *d_ptr = &x;
  T *t_ptr = (T *)d_ptr;
  T &t = *t_ptr;
  // DBG_VERBOSE("double2T x=", x, " t=", t, "\n");
  return t;
}

PromiseReduce::PromiseReduce(team &_team)
    : _vals{}
    , _results(Vals(), _team)
    , _ops{}
    , _roots{}
    , _proms{}
    , _fut_fulfilled(make_future()) {
  assert(!upcxx::in_progress());
}

PromiseReduce::~PromiseReduce() {
  if (upcxx::initialized()) {
    assert(!upcxx::in_progress());
    if (!_vals.empty() || !_fut_fulfilled.ready()) {
      LOG("PromiseReduce destructor found pending reductions\n");
      fulfill().wait();
    }
  } else if (!_vals.empty() || !_fut_fulfilled.ready()) {
    //SWARN("PromiseReduce was destroyed after upcxx::finalize with ", _vals.size(), " reductions pending or fulfilled not ready: ", _fut_fulfilled.ready(), "\n");
    assert(_vals.empty() && _fut_fulfilled.ready() && "All PromiseReduce options are completed");
  }
}

future<PromiseReduce::Vals> PromiseReduce::fulfill() {
  assert(!upcxx::in_progress());
  if (_in_fulfill) {
    LOG("Avoiding reentrant fulfill call for ", (void *)this, "\n");
    return make_future(Vals());
  }

  if (_vals.empty()) {
    DBG("No pending reductions for ", (void *)this, " ready?", _fut_fulfilled.ready(), "\n");
    _fut_fulfilled.wait();
    return make_future(Vals());
  }

  _in_fulfill = true;
  BaseTimer t;
  t.start();
  LOG("Waiting for pending fulfilled PromiseReduce reductions to complete through fulfilled_count=", get_fulfilled_count(), " for ",
      (void *)this, "\n");
  // ensure all previous fulfill calls have completed
  // ensure all reductions have been requested by all ranks (do not recursively use this PromiseReduce!)
  _fut_fulfilled.wait();
  // FIXME to use BarrierTimer and reductions that come with it
  barrier(get_team());
  assert(_fut_fulfilled.ready());
  t.stop();
  LOG("Waited ", t.get_elapsed(), "\n");

  auto sz = _vals.size();
  AsyncTimer fulfill_t("fulfill " + std::to_string(sz) + " reductions at global_count=" + std::to_string(get_global_count()) +
                       " fulfilled_count=" + std::to_string(get_fulfilled_count()));
  fulfill_t.start();

  assert(sz > 0);
  int power = roundup_log2(sz);
  assert(1 << power <= MAX_PROMISE_REDUCTIONS);
  auto max_sz = 1 << power;
  LOG("Using power=", power, " sz=", sz, " max_sz=", max_sz, "\n");

  shared_ptr<Funcs> sh_ops = make_shared<Funcs>();
  shared_ptr<Vals> sh_vals = make_shared<Vals>();
  shared_ptr<vector<int>> sh_roots = make_shared<vector<int>>();
  shared_ptr<Promises> sh_proms = make_shared<Promises>();

  assert(_results->empty());
  _results->reserve(max_sz);
  _results->resize(sz, std::numeric_limits<T>::max());

  assert(_vals.size() == sz);
  assert(_proms.size() == sz);
  assert(_ops.size() == sz);
  assert(_roots.size() == sz);
  assert(_results->size() == sz);
  // move to prepare PromiseReduction for another round
  sh_ops->swap(_ops);
  sh_vals->swap(_vals);
  sh_roots->swap(_roots);
  sh_proms->swap(_proms);
  auto sh_my_op = make_shared<op_PromiseReduce>(*sh_ops);

  _results->reserve(max_sz);
  sh_vals->reserve(max_sz);

  assert(sh_vals->size() == sz);
  assert(sh_roots->size() == sz);
  assert(sh_proms->size() == sz);
  assert(sh_ops->size() == sz);
  assert(&sh_my_op->_ops == sh_ops.get() && "Ops is a reference");

  // to be swapped once _results have been calculated until *after* reduction and/or broadcast
  shared_ptr<Vals> sh_results = make_shared<Vals>();

  // determine the operations with non-zero roots and reduce_all
  // that have different needs for syncing
  int count_reduce_all = 0, count_not_root = 0;
  for (auto root : *sh_roots) {
    if (root < 0)
      count_reduce_all++;
    else if (root > 0)
      count_not_root++;
  }

  get_fulfilled_count() += sz;
  LOG("Fulfilling ", sz, " reducts, ", count_reduce_all, " reduce_all and ", sz - count_reduce_all, " reduce_one with ",
      count_not_root, " on non-zero-root combined reduction. new fulfilled_count=", get_fulfilled_count(), " this=", (void *)this,
      "\n");

  assert(sh_vals->size() == _results->size());
  assert(sh_vals->size() == sz);
  assert(_results->size() == sz);
  assert(sh_my_op->_ops.size() == sz);

  // DBG_VERBOSE("sh_vals->", (void*) sh_vals->data(), " _results->", (void*) _results->data(), "\n");

  future<> fut_reduce;

  switch (power) {
    case (0):
      using T0 = std::array<T, 1>;
      assert(sz * sizeof(T) <= sizeof(T0));
      fut_reduce = upcxx::reduce_one<T0>((T0 *)sh_vals->data(), (T0 *)_results->data(), 1, *sh_my_op, 0, get_team());
      break;
    case (1):
      using T1 = std::array<T, 2>;
      assert(sz * sizeof(T) <= sizeof(T1));
      fut_reduce = upcxx::reduce_one<T1>((T1 *)sh_vals->data(), (T1 *)_results->data(), 1, *sh_my_op, 0, get_team());
      break;
    case (2):
      using T2 = std::array<T, 4>;
      assert(sz * sizeof(T) <= sizeof(T2));
      fut_reduce = upcxx::reduce_one<T2>((T2 *)sh_vals->data(), (T2 *)_results->data(), 1, *sh_my_op, 0, get_team());
      break;
    case (3):
      using T3 = std::array<T, 8>;
      assert(sz * sizeof(T) <= sizeof(T3));
      fut_reduce = upcxx::reduce_one<T3>((T3 *)sh_vals->data(), (T3 *)_results->data(), 1, *sh_my_op, 0, get_team());
      break;
    case (4):
      using T4 = std::array<T, 16>;
      assert(sz * sizeof(T) <= sizeof(T4));
      fut_reduce = upcxx::reduce_one<T4>((T4 *)sh_vals->data(), (T4 *)_results->data(), 1, *sh_my_op, 0, get_team());
      break;
    case (5):
      using T5 = std::array<T, 32>;
      assert(sz * sizeof(T) <= sizeof(T5));
      fut_reduce = upcxx::reduce_one<T5>((T5 *)sh_vals->data(), (T5 *)_results->data(), 1, *sh_my_op, 0, get_team());
      break;
    case (6):
      using T6 = std::array<T, 64>;
      assert(sz * sizeof(T) <= sizeof(T6));
      fut_reduce = upcxx::reduce_one<T6>((T6 *)sh_vals->data(), (T6 *)_results->data(), 1, *sh_my_op, 0, get_team());
      break;
    case (7):
      using T7 = std::array<T, 128>;
      assert(sz * sizeof(T) <= sizeof(T7));
      fut_reduce = upcxx::reduce_one<T7>((T7 *)sh_vals->data(), (T7 *)_results->data(), 1, *sh_my_op, 0, get_team());
      break;
    case (8):
      using T8 = std::array<T, 256>;
      assert(sz * sizeof(T) <= sizeof(T8));
      fut_reduce = upcxx::reduce_one<T8>((T8 *)sh_vals->data(), (T8 *)_results->data(), 1, *sh_my_op, 0, get_team());
      break;
    case (9):
      using T9 = std::array<T, 512>;
      assert(sz * sizeof(T) <= sizeof(T9));
      fut_reduce = upcxx::reduce_one<T9>((T9 *)sh_vals->data(), (T9 *)_results->data(), 1, *sh_my_op, 0, get_team());
      break;
    case (10):
      using T10 = std::array<T, 1024>;
      assert(sz * sizeof(T) <= sizeof(T10));
      fut_reduce = upcxx::reduce_one<T10>((T10 *)sh_vals->data(), (T10 *)_results->data(), 1, *sh_my_op, 0, get_team());
      break;
    case (11):
      using T11 = std::array<T, 2048>;
      assert(sz * sizeof(T) <= sizeof(T11));
      fut_reduce = upcxx::reduce_one<T11>((T11 *)sh_vals->data(), (T11 *)_results->data(), 1, *sh_my_op, 0, get_team());
      break;
    case (12):
      using T12 = std::array<T, 4096>;
      assert(sz * sizeof(T) <= sizeof(T12));
      fut_reduce = upcxx::reduce_one<T12>((T12 *)sh_vals->data(), (T12 *)_results->data(), 1, *sh_my_op, 0, get_team());
      break;
    case (13):
      using T13 = std::array<T, 8192>;
      assert(sz * sizeof(T) <= sizeof(T13));
      fut_reduce = upcxx::reduce_one<T13>((T13 *)sh_vals->data(), (T13 *)_results->data(), 1, *sh_my_op, 0, get_team());
      break;
    default: DIE("Cannot handle this yet"); break;
  }
  // fut_reduce = upcxx::reduce_one(sh_vals->data(), _results->data(), sz, *sh_my_op, 0, get_team());
  future<> fut_results = fut_reduce.then(
      [&self = *this, sz, sh_vals, sh_roots, sh_results, sh_proms, sh_ops, sh_my_op, count_reduce_all, count_not_root]() {
        future<> fut_chain = make_future();
        assert(sh_vals->size() == sz);
        assert(self._results->size() == sz);
        for (int i = 0; i < sz; i++) {
          if (count_reduce_all == 0 && count_not_root > 0 && self.get_team().rank_me() == 0) {
            // no broadcast and some non-zero roots need a rpc to complete
            assert((*self._results)[i] != std::numeric_limits<T>::max());
            DBG_VERBOSE("Reduced ", i, " val=", (*sh_vals)[i], " result=", (*self._results)[i], " root=", (*sh_roots)[i], "\n");
            assert((*sh_roots)[i] >= 0 && "not a reduce-all situation");
            // send result to the desired rank if it is not rank 0
            if ((*sh_roots)[i] != 0) {
              LOG("Sending result=", (*self._results)[i], " to root=", (*sh_roots)[i], "\n");
              auto fut = rpc(
                  self.get_team(), (*sh_roots)[i],
                  [](dist_object<Vals> &results, int i, T result) {
                    DBG_VERBOSE("Got result=", result, " for i=", i, "\n");
                    (*results)[i] = result;
                  },
                  self._results, i, (*self._results)[i]);
              fut_chain = when_all(fut, fut_chain);
            }
          }
        }
        DBG_VERBOSE("Finished reduction\n");
        return when_all(fut_chain);
      });

  if (count_reduce_all > 0 || count_not_root > 0) {
    // all ranks must wait for reduction to complete
    LOG("Waiting for actual reduction to complete so broadcast / rpcs are completed before progressing\n");
    fut_results.wait();
    barrier(get_team());
    // now swap the dist_object results with the sh_results to be downstream outside
    sh_results->swap(*_results);
    assert(_results->empty());
    assert(sh_results->size() == sz);
    assert(sh_vals->size() == sz);
    if (count_reduce_all > 0) {
      LOG("Broadcasting ", sh_vals->size(), " results\n");
      fut_results = upcxx::broadcast(sh_results->data(), sz, 0, get_team());
    }
  } else {
    // sh_results can swap anytime after reductions complete
    fut_results = fut_results.then([sh_results, sz, &self = *this]() {
      sh_results->swap(*self._results);
      assert(self._results->empty());
      assert(sh_results->size() == sz);
    });
  }

  assert(_ops.empty());
  assert(_vals.empty());
  assert(_roots.empty());
  assert(_proms.empty());

  fut_results = fut_results.then([fulfill_t, sh_vals, sh_results, sh_ops, sh_my_op]() { fulfill_t.stop(); });

  auto fut_ret = fut_results.then([&_team = get_team(), sh_results, sh_proms, sh_roots, sz]() {
    assert(sh_results->size() == sz);
    assert(sh_proms->size() == sz);
    assert(sh_roots->size() == sz);
    for (int i = 0; i < sh_results->size(); i++) {
      promise<T> &prom = *((*sh_proms)[i]);
      prom.fulfill_result((*sh_results)[i]);
    }
    return *(sh_results);
  });

  _fut_fulfilled = fut_ret.then([](Vals ignore) {});
  Timings::set_pending(_fut_fulfilled);
  _in_fulfill = false;
  return fut_ret;
}

};  // namespace upcxx_utils
