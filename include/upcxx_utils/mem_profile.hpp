#pragma once

#include <fstream>
#include <utility>
#include <vector>

#include "log.hpp"
#include "timers.hpp"
#include "version.h"

#ifndef UPCXX_UTILS_NO_THREADS
#include <thread>
#endif

#ifndef UPCXX_UTILS_LOG_FLUSH_INTERVAL_MS
#define UPCXX_UTILS_LOG_FLUSH_INTERVAL_MS 60000.
#endif

#ifndef UPCXX_UTILS_SAMPLE_INTERVAL_MS
#define UPCXX_UTILS_SAMPLE_INTERVAL_MS 500
#endif

#include <upcxx/upcxx.hpp>

namespace upcxx_utils {

extern ofstream _memlog;
void open_memlog(string name);
void close_memlog();

std::pair<double, std::string> get_free_mem(const std::vector<std::string> &);
double get_free_mem(bool local_barrier = false);
string get_self_stat(void);

#define _TRACK_MEMORY(free_mem, self_stat) " Free memory: ", ::upcxx_utils::get_size_str(free_mem), ", stat: ", self_stat

#define LOG_MEM_OS(os, label)                                             \
  ::upcxx_utils::logger(os, false, false, true, LOG_LINE_TS_LABEL, label, \
                        _TRACK_MEMORY(::upcxx_utils::get_free_mem(), ::upcxx_utils::get_self_stat()))

#define LOG_MEM(label)                                                                                             \
  do {                                                                                                             \
    std::string __label(label);                                                                                    \
    if (::upcxx_utils::_memlog.is_open()) {                                                                        \
      LOG_MEM_OS(::upcxx_utils::_memlog, __label);                                                                 \
      ::upcxx_utils::_memlog.flush();                                                                              \
    } else if (::upcxx_utils::_logstream.is_open()) {                                                              \
      LOG_MEM_OS(::upcxx_utils::_logstream, __label);                                                              \
    } else if (::upcxx_utils::_dbgstream.is_open()) {                                                              \
      LOG_MEM_OS(::upcxx_utils::_dbgstream, __label);                                                              \
    }                                                                                                              \
    upcxx::barrier(upcxx::local_team());                                                                           \
    auto fut_free_mem = upcxx_utils::min_sum_max_reduce_one(::upcxx_utils::get_free_mem() / ONE_GB);               \
    upcxx::barrier(upcxx::local_team());                                                                           \
    upcxx::future<> __fut_log_mem =                                                                                \
        upcxx::when_all(Timings::get_pending(), fut_free_mem).then([__label](upcxx_utils::MinSumMax<double> msm) { \
          SLOG_VERBOSE(__label, " free_mem GB: ", msm.to_string(), "\n");                                          \
        });                                                                                                        \
    Timings::set_pending(__fut_log_mem);                                                                           \
  } while (0)

#ifndef UPCXX_UTILS_NO_THREADS
class MemoryTrackerThread {
  std::thread *t = nullptr;
  double start_free_mem, min_free_mem;
  long ticks, sample_ms;
  bool fin,opened;
  std::string tracker_filename;

 public:
  MemoryTrackerThread(string _tracker_filename = "")
      : t{}
      , ticks{0}
      , sample_ms{UPCXX_UTILS_SAMPLE_INTERVAL_MS}
      , fin{false}
      , opened{false}
      , tracker_filename(_tracker_filename) {}
  MemoryTrackerThread(const MemoryTrackerThread &) = delete; // no copy
  MemoryTrackerThread(MemoryTrackerThread &&) = default;
  void start();
  void stop();
};
#endif

};  // namespace upcxx_utils
