#pragma once
#include <functional>
#include <typeinfo>
#include <vector>

#include "min_sum_max.hpp"
#include "upcxx/upcxx.hpp"

using std::function;
using std::pair;
using std::shared_ptr;
using std::string;
using std::vector;

using upcxx::dist_object;
using upcxx::intrank_t;
using upcxx::make_future;
using upcxx::op_fast_add;
using upcxx::op_fast_max;
using upcxx::op_fast_min;
using upcxx::promise;
using upcxx::team;
using upcxx::when_all;
using upcxx::world;

#ifndef MAX_PROMISE_REDUCTIONS
#define MAX_PROMISE_REDUCTIONS 1024
#endif

namespace upcxx_utils {

int roundup_log2(uint64_t n);

class PromiseBarrier {
  // a two step barrier with delayed execution
  //
  // construct in the master thread (with strict ordering of collectives on team)
  // fulfill() and get_future() methods are safe to call within the restricted context
  // of a progress callback
  //
  // implements the Dissemination Algorithm
  //   detailed in "Two algorithms for Barrier Synchronization"
  //   Manber et al, 1998
  // O(logN) latency
  // O(NlogN) total small messages
  //
  // Usage:
  // PromiseBarrier prom_barrier(team);
  // ...
  // prom_barrier.fulfill();  // may call within progess() callback
  // ...
  // prom_barrier.get_future().then(...); // may call within progress() callback
  //
  struct DisseminationWorkflow;
  using DistDisseminationWorkflow = dist_object<DisseminationWorkflow>;
  struct DisseminationWorkflow {
    static void init_workflow(DistDisseminationWorkflow &dist_dissem);

    DisseminationWorkflow();
    upcxx::future<> get_future() const;

    vector<upcxx::promise<>> level_proms;  // one for each level instance
    upcxx::promise<> initiated_prom;       // to signal this rank start
    upcxx::future<> done_future;
  };

  const upcxx::team &tm;
  DistDisseminationWorkflow dist_workflow;
  bool moved = false;

 public:
  PromiseBarrier(const upcxx::team &tm = upcxx::world());
  PromiseBarrier(PromiseBarrier &&move);
  PromiseBarrier(const PromiseBarrier &copy) = delete;
  PromiseBarrier &operator=(PromiseBarrier &&move);
  PromiseBarrier &operator=(const PromiseBarrier &copy) = delete;
  ~PromiseBarrier();
  void fulfill() const;
  upcxx::future<> get_future() const;
};

// PromiseReduce functor

struct op_PromiseReduce {
  using T = int64_t;
  using Func = std::function<T(T, T)>;
  using Funcd = std::function<double(double, double)>;
  using BothFunc = std::pair<Func, Funcd>;
  using FuncBool = std::pair<BothFunc, bool>;
  using Funcs = vector<FuncBool>;
  Funcs &_ops;

  // get_op() rotates through the operations each time operator() is called
  // FuncBool &get_op() const { return _ops[_rrobin++ % _ops.size()]; }
  op_PromiseReduce(Funcs &ops);
  op_PromiseReduce() = delete;
  op_PromiseReduce(const op_PromiseReduce &copy);
  static double &T2double(T &x);
  static T &double2T(double &x);

  // Each operation will be applied to an array where each operation could be different
  template <typename Ta, typename Tb>
  Ta operator()(Ta &__a, Tb &&__b) const {
    T *_a = const_cast<T *>(__a.data());
    T *_b = const_cast<T *>(__b.data());
    // DBG_VERBOSE("Operating on ", _ops.size(), " elements at ", (void*) _a, " ", (void*) _b, " this=", (void*) this, "\n");
    for (int i = 0; i < _ops.size(); i++) {
      T &a = _a[i];
      T &b = _b[i];
      FuncBool &op = _ops[i];
      // DBG_VERBOSE("Operating ", typeid(op.first).name(), " ", op.second ? " double " : " long ", " on a=", a, " b=", b, "\n");
      if (op.second) {
        double &a_d = T2double(a);
        double &b_d = T2double(b);
        double a_d2 = op.first.second(a_d, b_d);
        // DBG_VERBOSE("a_d=", a_d, " b_d=", b_d, " a_d2=", a_d2, "\n");
        a = double2T(a_d2);
      } else {
        a = op.first.first(a, std::forward<T>(b));
      }
      // DBG_VERBOSE("Result a=", a, "\n");
    }
    return static_cast<Ta &&>(__a);
  };
};

class PromiseReduce {
  // PromiseReduce
  //
  // a class to consolidate and delay a series of arbitrary reductions.
  // All values are converted to uint64_t (doubles are first bit-smashed into an int64_t and un-smashed when operating on and
  // returned)
  //   bit smashing is facilitated by op_PromiseReduce::T2double and op_PromiseReduce::double2T
  //   The aggregate return contains the double bit-smashed into int64_t, but the individual return type is the original type that
  //   reduce_* was called on.
  // Any series of binary operators is allowed
  //   ranks may initiate reduce from within the restricted contex
  //   **BUT** the order of reduce_one and reduce_all calls must be consistant across ranks
  // if any of the ops are reduce_all then all the values are broadcasted after the reduction.
  // or if all the ops are reduce_one and the root is not rank0 for a given operation, then a rpc is issued to get the value to the
  // correct rank
  //
  // fulfill can be called any number of times, and trigger a reduction for all pending reduce_one and reduce_all operations
  // fulfill can also not be called at all and will trigger all reductions before the PromiseReduce instance is destroyed
  //
  // Usage:
  //  PromiseReduce pr(local_team());  // create a PromiseReduce working on the local_team
  //  auto fut1 = pr.reduce_one(rank_me(), op_fast_max, 0); // max to rank 0
  //  auto fut2 = pr.reduce_one(rank_me(), op_fast_add, local_team().rank_n()-1); // sum to last rank on node
  //  auto fut3 = pr.reduce_all(rank_me(), op_fast_add); // sum to all ranks
  //  auto fut4 = pr.reduce_all(1.0/rank_me(), op_fast_add); // sum double to all ranks
  //  auto fut_4_reductions = pr.fulfill();
  //  assert(fut_4_redutions.wait().size() == 4);
  //  auto [ int1, int2, int3, double4 ] = fut_4_reductions.wait();
  //  assert(fut1.wait() == int1);
  //  assert(fut2.wait() == int2);
  //  assert(fut3.wait() == int3);
  //  assert(fabs(fut4.wait() - op_PromiseReduce::T2double(double4)) < 0.001);
  //  pr.reduce_all(rank_me(), op_fast_add);
  //  auto fut1_reduction = pr.fulfill(); // instance can be used to initate more reductions
  //  assert(fut_reduction.wait().size() == 1);

 public:
  using T = op_PromiseReduce::T;
  using Funcs = op_PromiseReduce::Funcs;
  using Promises = vector<shared_ptr<promise<T>>>;
  using Vals = vector<T>;

 protected:
  Vals _vals;
  dist_object<Vals> _results;
  op_PromiseReduce::Funcs _ops;
  vector<int> _roots;
  Promises _proms;
  upcxx::future<> _fut_fulfilled;
  bool _in_fulfill = false;

 public:
  PromiseReduce(team &_team = world());

  ~PromiseReduce();

  static size_t &get_global_count() {
    static size_t _ = 0;
    return _;
  }

  static size_t &get_fulfilled_count() {
    static size_t _ = 0;
    return _;
  }

  const upcxx::team &get_team() const { return _results.team(); }

  template <typename ValType, typename Op>
  upcxx::future<ValType> reduce_one(ValType orig_val, Op &op, int root = 0) {
    auto fut_fulfill = make_future();
    if (!upcxx::in_progress() && _vals.size() + 1 > MAX_PROMISE_REDUCTIONS) {
      LOG("Triggering fulfill as ", _vals.size(), " >= ", MAX_PROMISE_REDUCTIONS, " reductions are pending\n");
      fut_fulfill = fulfill().then([](Vals ignore) {});
    }
    T val;
    bool is_float = false;
    if (std::is_floating_point<ValType>::value) {
      // val = (T)(orig_val * TRANSFORM_FLOAT);
      double conv_val = orig_val;  // upscale floats to doubles
      val = op_PromiseReduce::double2T(conv_val);
      is_float = true;
    } else
      val = orig_val;

    LOG("Added reduce_", (root < 0 ? "all" : "one"), " op #", _vals.size(), " ", typeid(op).name(), " ", (void *)&op, "on ",
        typeid(ValType).name(), " val=", val, " orig_val=", orig_val, ". global_count=", get_global_count(),
        " fulfilled_count=", get_fulfilled_count(), " this=", (void *)this, "\n");
    get_global_count()++;
    auto sh_prom = make_shared<promise<T>>();
    _vals.reserve(MAX_PROMISE_REDUCTIONS);
    _proms.reserve(MAX_PROMISE_REDUCTIONS);
    _ops.reserve(MAX_PROMISE_REDUCTIONS);
    _roots.reserve(MAX_PROMISE_REDUCTIONS);
    _vals.push_back(val);
    _proms.push_back(sh_prom);
    _ops.push_back({{op, op}, is_float});
    _roots.push_back(root);
    auto fut_ret = sh_prom->get_future().then([&self = *this, sh_prom, is_float, sz = _vals.size(), root](T result) {
      ValType conv_result = result;
      if (std::is_floating_point<ValType>::value) {
        assert(is_float);
        double double_result = op_PromiseReduce::T2double(result);
        conv_result = double_result;  // downscale doubles to floats if needed
        DBG_VERBOSE("Converted reduce_", (root < 0 ? "all" : "one"), " op #", sz - 1, " from ", result, " back to ", conv_result,
                    "\n");
      } else {
        assert(!is_float);
      }
      DBG_VERBOSE("Returning ", conv_result, " sh_prom:", (void *)sh_prom.get(), "\n");
      return conv_result;
    });
    return when_all(fut_fulfill, fut_ret);
  };

  template <typename ValType, typename Op>
  upcxx::future<> reduce_one(ValType *in_vals, ValType *out_vals, int count, Op &op, int root = 0) {
    upcxx::future<> chain_fut = make_future();
    for (int i = 0; i < count; count++) {
      auto fut_val = reduce_one(in_vals[i], op, root).then([&out = out_vals[i]](ValType val) { return out = val; });
      chain_fut = when_all(chain_fut, fut_val.then([](ValType ignore) {}));
    }
    return chain_fut;
  };

  template <typename ValType, typename Op>
  upcxx::future<ValType> reduce_all(ValType orig_val, Op &op) {
    return reduce_one(orig_val, op, -1);
  };

  template <typename ValType, typename Op>
  upcxx::future<> reduce_all(ValType *in_vals, ValType *out_vals, int count, Op &op) {
    return reduce_one(in_vals, out_vals, count, op, -1);
  };

  template <typename ValType>
  upcxx::future<MinSumMax<ValType>> msm_reduce_one(ValType orig_val, int root = 0) {
    auto fut_fulfill = make_future();
    if (!upcxx::in_progress() && _vals.size() + 4 > MAX_PROMISE_REDUCTIONS) {
      LOG("Triggering fulfill as ", _vals.size(), " >= ", MAX_PROMISE_REDUCTIONS,
          " reductions are pending for this MinSumMax reduction\n");
      fut_fulfill = fulfill().then([](Vals ignore) {});
    }
    auto fut_my = make_future(orig_val);
    auto fut_min = reduce_one(orig_val, op_fast_min, root);
    auto fut_sum = reduce_one(orig_val, op_fast_add, root);
    auto fut_max = reduce_one(orig_val, op_fast_max, root);
    return when_all(fut_fulfill, fut_my, fut_min, fut_sum, fut_max).then([this](ValType my, ValType min, ValType sum, ValType max) {
      MinSumMax<ValType> msm;
      msm.my = my;
      msm.min = min;
      msm.sum = sum;
      msm.max = max;
      msm.apply_avg(this->get_team());
      return msm;
    });
  };

  template <typename ValType>
  upcxx::future<MinSumMax<ValType>> msm_reduce_all(ValType orig_val) {
    return msm_reduce_one(orig_val, -1);
  };

  template <typename T>
  upcxx::future<> msm_reduce_one(const MinSumMax<T> *msm_in, MinSumMax<T> *msm_out, int count, intrank_t root = 0) {
    upcxx::future<> chain_fut = make_future();
    for (int i = 0; i < count; i++) {
      auto &out = msm_out[i];
      auto &in = msm_in[i];
      auto fut_msm = msm_reduce_one(msm_in[i].my, root).then([&in, &out](MinSumMax<T> msm) { out = msm; });
      chain_fut = when_all(chain_fut, fut_msm);
    }
    return chain_fut;
  }
  template <typename T>
  upcxx::future<> msm_reduce_all(const MinSumMax<T> *msm_in, MinSumMax<T> *msm_out, int count) {
    return msm_reduce_one(msm_in, msm_out, count, -1);
  }

  upcxx::future<Vals> fulfill();
};

};  // namespace upcxx_utils
