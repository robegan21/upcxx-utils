#include <random>
#include <vector>

#include "upcxx/upcxx.hpp"
#include "upcxx_utils/log.hpp"
#include "upcxx_utils/promise_collectives.hpp"

using std::vector;

using upcxx::barrier;
using upcxx::future;
using upcxx::make_future;
using upcxx::when_all;

using upcxx_utils::op_PromiseReduce;
using upcxx_utils::PromiseBarrier;
using upcxx_utils::PromiseReduce;
using upcxx_utils::roundup_log2;

#define ASSERT_FLOAT(a, b, c) assert((a) >= ((b)-0.000001) && (a) <= ((b) + 0.000001) && c)

int test_promise_barrier(int argc, char **argv) {
  barrier();
  upcxx_utils::open_dbg("test_promise_barrier");

  assert(roundup_log2(0) == -1);
  assert(roundup_log2(1) == 0);
  assert(roundup_log2(2) == 1);
  assert(roundup_log2(3) == 2);
  assert(roundup_log2(4) == 2);
  assert(roundup_log2(5) == 3);
  assert(roundup_log2(6) == 3);
  assert(roundup_log2(7) == 3);
  assert(roundup_log2(8) == 3);
  assert(roundup_log2(9) == 4);
  assert(roundup_log2(15) == 4);
  assert(roundup_log2(16) == 4);
  assert(roundup_log2(17) == 5);
  {
    PromiseBarrier pb;
    assert(!pb.get_future().ready());
    pb.fulfill();
    pb.get_future().wait();
  }
  barrier();
  {
    DBG("1s 2s 1e 2e\n");
    barrier();
    PromiseBarrier pb1, pb2;
    barrier();
    assert(!pb1.get_future().ready());
    pb1.fulfill();
    barrier();
    assert(!pb2.get_future().ready());
    pb2.fulfill();
    barrier();
    pb1.get_future().wait();
    barrier();
    pb2.get_future().wait();
    barrier();
  }
  {
    DBG("1s 1e 2s 2e\n");
    barrier();
    PromiseBarrier pb1, pb2;
    barrier();
    assert(!pb1.get_future().ready());
    pb1.fulfill();
    barrier();
    pb1.get_future().wait();
    barrier();
    assert(!pb2.get_future().ready());
    pb2.fulfill();
    barrier();
    pb2.get_future().wait();
    barrier();
  }
  {
    DBG("1s 2s 2e 1e\n");
    barrier();
    PromiseBarrier pb1, pb2;
    barrier();
    assert(!pb1.get_future().ready());
    pb1.fulfill();
    barrier();
    assert(!pb2.get_future().ready());
    pb2.fulfill();
    barrier();
    pb2.get_future().wait();
    barrier();
    pb1.get_future().wait();
    barrier();
  }

  {
    DBG("2s 1s 2e 1e\n");
    barrier();
    PromiseBarrier pb1, pb2;
    barrier();
    assert(!pb2.get_future().ready());
    pb2.fulfill();
    barrier();
    assert(!pb1.get_future().ready());
    pb1.fulfill();
    barrier();
    pb2.get_future().wait();
    barrier();
    pb1.get_future().wait();
    barrier();
  }
  {
    DBG("2s 2e 1s 1e\n");
    barrier();
    PromiseBarrier pb1, pb2;
    barrier();
    assert(!pb2.get_future().ready());
    pb2.fulfill();
    barrier();
    pb2.get_future().wait();
    barrier();
    assert(!pb1.get_future().ready());
    pb1.fulfill();
    barrier();
    pb1.get_future().wait();
    barrier();
  }
  {
    DBG("2s 1s 1e 2e\n");
    barrier();
    PromiseBarrier pb1, pb2;
    barrier();
    assert(!pb2.get_future().ready());
    pb2.fulfill();
    barrier();
    assert(!pb1.get_future().ready());
    pb1.fulfill();
    barrier();
    pb1.get_future().wait();
    barrier();
    pb2.get_future().wait();
    barrier();
  }

  std::mt19937 g(rank_n());  // seed all ranks the same
  {
    DBG("fulfill all barrier wait all same order\n");
    int iterations = 1000;
    vector<PromiseBarrier> pbs(iterations);
    vector<intrank_t> fulfill_order(iterations);
    vector<intrank_t> &wait_order = fulfill_order;
    for (int i = 0; i < iterations; i++) {
      fulfill_order[i] = i;
      wait_order[i] = i;
      assert(!pbs[i].get_future().ready());
    }
    std::shuffle(fulfill_order.begin(), fulfill_order.end(), g);
    barrier();
    // initiate all
    for (int i = 0; i < iterations; i++) {
      assert(!pbs[fulfill_order[i]].get_future().ready());
      pbs[fulfill_order[i]].fulfill();
    }
    // wait all
    future<> all_fut = make_future();
    for (int i = 0; i < iterations; i++) {
      auto fut = pbs[wait_order[i]].get_future();
      all_fut = when_all(all_fut, fut);
    }
    all_fut.wait();
    barrier();
  }

  {
    DBG("fulfill all barrier wait all different order\n");
    int iterations = 1000;
    vector<PromiseBarrier> pbs(iterations);
    vector<intrank_t> fulfill_order(iterations);
    vector<intrank_t> wait_order(iterations);
    for (int i = 0; i < iterations; i++) {
      fulfill_order[i] = i;
      wait_order[i] = i;
      assert(!pbs[i].get_future().ready());
    }
    std::shuffle(fulfill_order.begin(), fulfill_order.end(), g);
    std::shuffle(wait_order.begin(), wait_order.end(), g);
    barrier();
    // initiate all
    for (int i = 0; i < iterations; i++) {
      assert(!pbs[fulfill_order[i]].get_future().ready());
      pbs[fulfill_order[i]].fulfill();
    }
    barrier();
    // wait all
    future<> all_fut = make_future();
    for (int i = 0; i < iterations; i++) {
      auto fut = pbs[wait_order[i]].get_future();
      all_fut = when_all(all_fut, fut);
    }
    all_fut.wait();
    barrier();
  }

  {
    DBG("fulfill all barrier wait each same order\n");
    int iterations = 1000;
    vector<PromiseBarrier> pbs(iterations);
    vector<intrank_t> fulfill_order(iterations);
    vector<intrank_t> &wait_order = fulfill_order;
    for (int i = 0; i < iterations; i++) {
      fulfill_order[i] = i;
      wait_order[i] = i;
    }
    std::shuffle(fulfill_order.begin(), fulfill_order.end(), g);
    barrier();
    // initiate all
    for (int i = 0; i < iterations; i++) {
      assert(!pbs[fulfill_order[i]].get_future().ready());
      pbs[fulfill_order[i]].fulfill();
    }
    barrier();
    // wait each
    for (int i = 0; i < iterations; i++) {
      pbs[wait_order[i]].get_future().wait();
    }
    barrier();
  }

  {
    DBG("fulfill all barrier wait each different order\n");
    int iterations = 1000;
    vector<PromiseBarrier> pbs(iterations);
    vector<intrank_t> fulfill_order(iterations);
    vector<intrank_t> wait_order(iterations);
    for (int i = 0; i < iterations; i++) {
      fulfill_order[i] = i;
      wait_order[i] = i;
    }
    std::shuffle(fulfill_order.begin(), fulfill_order.end(), g);
    std::shuffle(wait_order.begin(), wait_order.end(), g);
    barrier();
    // initiate all
    for (int i = 0; i < iterations; i++) {
      assert(!pbs[fulfill_order[i]].get_future().ready());
      pbs[fulfill_order[i]].fulfill();
    }
    barrier();
    // wait each
    for (int i = 0; i < iterations; i++) {
      pbs[wait_order[i]].get_future().wait();
    }
    barrier();
  }

  {
    DBG("fulfill then wait each same order\n");
    int iterations = 1000;
    vector<PromiseBarrier> pbs(iterations);
    vector<intrank_t> fulfill_order(iterations);
    vector<intrank_t> &wait_order = fulfill_order;
    for (int i = 0; i < iterations; i++) {
      fulfill_order[i] = i;
      wait_order[i] = i;
    }
    std::shuffle(fulfill_order.begin(), fulfill_order.end(), g);
    barrier();
    // initiate all
    for (int i = 0; i < iterations; i++) {
      assert(!pbs[fulfill_order[i]].get_future().ready());
      pbs[fulfill_order[i]].fulfill();
      pbs[wait_order[i]].get_future().wait();
    }
    barrier();
  }

  DBG("fulfill then wait each different order would deadlock\n");

  {
    DBG("fulfill all then wait all same order\n");
    int iterations = 1000;
    vector<PromiseBarrier> pbs(iterations);
    vector<intrank_t> fulfill_order(iterations);
    vector<intrank_t> &wait_order = fulfill_order;
    for (int i = 0; i < iterations; i++) {
      fulfill_order[i] = i;
      wait_order[i] = i;
    }
    std::shuffle(fulfill_order.begin(), fulfill_order.end(), g);
    barrier();
    // initiate all
    future<> all_fut = make_future();
    for (int i = 0; i < iterations; i++) {
      assert(!pbs[fulfill_order[i]].get_future().ready());
      pbs[fulfill_order[i]].fulfill();
      auto fut = pbs[wait_order[i]].get_future();
      all_fut = when_all(all_fut, fut);
    }
    all_fut.wait();
    barrier();
  }

  {
    DBG("fulfill all then wait all different order\n");
    int iterations = 1000;
    vector<PromiseBarrier> pbs(iterations);
    vector<intrank_t> fulfill_order(iterations);
    vector<intrank_t> wait_order(iterations);
    for (int i = 0; i < iterations; i++) {
      fulfill_order[i] = i;
      wait_order[i] = i;
    }
    std::shuffle(fulfill_order.begin(), fulfill_order.end(), g);
    std::shuffle(wait_order.begin(), wait_order.end(), g);
    barrier();
    // initiate all
    future<> all_fut = make_future();
    for (int i = 0; i < iterations; i++) {
      assert(!pbs[fulfill_order[i]].get_future().ready());
      pbs[fulfill_order[i]].fulfill();
      auto fut = pbs[wait_order[i]].get_future();
      all_fut = when_all(all_fut, fut);
    }
    all_fut.wait();
    barrier();
  }

  upcxx_utils::close_dbg();
  barrier();
  return 0;
}

int test_promise_reduce(int argc, char **argv) {
  barrier();
  upcxx_utils::open_dbg("test_promise_reduce");

  {
    double d1 = 2.0, d2 = 3.14159265358979;
    int64_t tmp = op_PromiseReduce::double2T(d1);
    ASSERT_FLOAT(op_PromiseReduce::T2double(tmp), d1, "");
    tmp = op_PromiseReduce::double2T(d2);
    ASSERT_FLOAT(op_PromiseReduce::T2double(tmp), d2, "");
  }

  {
    PromiseReduce pr /*mixed*/, pr_2 /*only reduce_all*/, pr_3 /*only reduce_one*/;
    auto fut_min = pr.reduce_one(rank_me(), op_fast_min);
    auto fut_a_min = pr_2.reduce_all(rank_me(), op_fast_min);
    auto fut_max = pr.reduce_one(rank_me(), op_fast_max);
    auto fut_a_max = pr.reduce_all(rank_me(), op_fast_max);
    auto fut_sum = pr.reduce_one(rank_me(), op_fast_add);
    auto fut_a_sum = pr.reduce_all(rank_me(), op_fast_add);

    auto fut_min_d = pr.reduce_one(rank_me() * 1.0 / rank_n(), op_fast_min);
    auto fut_a_min_d = pr.reduce_all(rank_me() * 1.0 / rank_n(), op_fast_min);
    auto fut_max_d = pr.reduce_one(rank_me() * 1.0 / rank_n(), op_fast_max);
    auto fut_a_max_d = pr.reduce_all(rank_me() * 1.0 / rank_n(), op_fast_max);
    auto fut_sum_d = pr.reduce_one(rank_me() * 1.0 / rank_n(), op_fast_add);
    auto fut_a_sum_d = pr_2.reduce_all(rank_me() * 1.0 / rank_n(), op_fast_add);
    auto fut_max_d2 = pr.reduce_one(rank_me() * 1.0 / rank_n(), op_fast_max);

    auto fut_min1 = pr_3.reduce_one(rank_me(), op_fast_min);
    auto fut_max1 = pr_3.reduce_one(rank_me(), op_fast_max);
    auto fut_sum1 = pr_3.reduce_one(rank_me(), op_fast_add);

    auto sum = 0;
    for (int i = 0; i < rank_n(); i++) sum += i;
    if (!rank_me()) INFO("sum = ", sum, "\n");

    auto results3 = pr_3.fulfill().wait();
    assert(results3.size() == 3);
    if (rank_me() == 0) {
      assert(results3[0] == fut_min1.wait());
      assert(results3[1] == fut_max1.wait());
      assert(results3[2] == sum);
      assert(results3[2] == fut_sum1.wait());
    }

    auto fut_results = pr.fulfill();
    auto x = fut_results.wait();
    assert(x.size() == 11);

    if (rank_me() == 0) {
      // INFO("fut_min=", fut_min.wait(), " fut_max=", fut_max.wait(), "\n");
      assert(fut_min.wait() == 0 && "min in 0");
      assert(fut_max.wait() == rank_n() - 1 && "max is last rank");
      // INFO("sum=", sum, " fut_sum=", fut_sum.wait(), "\n");
      assert(fut_sum.wait() == sum && "sum is correct");
      //INFO("fut_min_d=", fut_min_d.wait(), " fut_max_d=", fut_max_d.wait(), " fut_sum_d=", fut_sum_d.wait(), "\n");
      ASSERT_FLOAT(fut_min_d.wait(), 0.0, "min in 0.0");
      ASSERT_FLOAT(fut_max_d.wait(), (rank_n() - 1.0) / (double)rank_n(), "max is last rank");
      ASSERT_FLOAT(fut_sum_d.wait(), sum / (double)rank_n(), "sum is correct");
      ASSERT_FLOAT(fut_max_d2.wait(), (rank_n() - 1.0) / (double)rank_n(), "max is last rank");
    }
    barrier();
    auto fut_all_results = pr_2.fulfill();
    auto y = fut_all_results.wait();
    assert(y.size() == 2);
    INFO("fut_a_min=", fut_a_min.wait(), " fut_a_max=", fut_a_max.wait(), "\n");
    assert(fut_a_min.wait() == 0 && "min in 0");
    assert(fut_a_max.wait() == rank_n() - 1 && "max is last rank");
    // INFO("sum=", sum, " fut_sum=", fut_sum.wait(), "\n");
    assert(fut_a_sum.wait() == sum && "sum is correct");
    // INFO("fut_min_d=", fut_a_min_d.wait(), " fut_max_d=", fut_a_max_d.wait(), " fut_sum_d=", fut_a_sum_d.wait(), "\n");
    ASSERT_FLOAT(fut_a_min_d.wait(), 0.0, "min in 0.0");
    ASSERT_FLOAT(fut_a_max_d.wait(), (rank_n() - 1.0) / (double)rank_n(), "max is last rank");
    ASSERT_FLOAT(fut_a_sum_d.wait(), sum / (double)rank_n(), "sum is correct");

    auto lme = local_team().rank_me();
    auto ln = local_team().rank_n();
    auto my_node = rank_me() / ln;
    INFO("lme=", lme, " ln=", ln, " my_node=", my_node, "\n");
    PromiseReduce pr3(local_team());
    auto tgt_root = ln / 2;
    pr3.reduce_one(rank_me(), op_fast_max, tgt_root);
    auto fut_msm = pr3.msm_reduce_one(rank_me());
    auto fut_pr3 = pr3.fulfill();
    if (lme == 0) {
      auto msm = fut_msm.wait();
      assert(msm.my == rank_me());
      assert(msm.min == rank_me());
      assert(msm.max == rank_me() + ln - 1);
    }

    PromiseReduce pr4(local_team());
    pr4.reduce_one(rank_me(), op_fast_max, tgt_root);
    pr4.reduce_all(rank_me(), op_fast_max);
    auto fut_pr4 = pr4.fulfill();

    assert(fut_pr3.wait().size() == 1 + 3);
    if (lme == tgt_root) {
      INFO("got=", fut_pr3.wait()[0], " exp=", (my_node + 1) * ln - 1, "\n");
      assert(fut_pr3.wait()[0] == (my_node + 1) * ln - 1 && "world rank of last rank in my local team");
    }
    assert(fut_pr4.wait().size() == 2);
    if (lme == tgt_root) assert(fut_pr4.wait()[0] == (my_node+1) * ln - 1 && "target root got world rank of last rank in my local team");
    assert(fut_pr4.wait()[1] == (my_node+1) * ln - 1 && "all ranks got world rank of last rank in my local team");
  }

  upcxx_utils::close_dbg();
  barrier();
  return 0;
}

int test_promise_collectives(int argc, char **argv) {
  test_promise_barrier(argc, argv);
  test_promise_reduce(argc, argv);
  return 0;
}
